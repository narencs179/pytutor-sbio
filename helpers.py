import os
import inspect
import sys
import time

script_path = os.path.realpath(__file__)
tutorial_dir = os.path.dirname(script_path)
data_dir = os.path.join(tutorial_dir, "data")

os.sys.path.append(tutorial_dir)

import bioseq

def test_check(test_result, msg, num = None):
    fmsg = f"{msg}: {test_result}"
    if num is not None:
        fmsg = f"Test {num:02d} - {fmsg}"
    print(fmsg)

def grandparent_locals():
    return inspect.currentframe().f_back.f_back.f_locals

def assignment_check_helper(vdict, answers):
    npassed = 0
    for i in range(len(answers)):
        answer = answers[i]
        var = answer[0]
        val = answer[1]
        ques = f"Q.{answer[2]}"
        if var in vdict:
            attempt = f"{ques} has been attempted ..."
            print(attempt, end = "\r")
            time.sleep(0.25)
            if var in vdict:
                if callable(vdict[var]):
                    if eval(val, vdict):
                        print(f"{attempt} Passed! ✅")
                        npassed = npassed + 1
                    else:
                        print(f"{attempt} Please try again. ❌")
                elif vdict[var] == val:
                    print(f"{attempt} Passed! ✅")
                    npassed = npassed + 1
                else:
                    print(f"{attempt} Please try again. ❌")
        else:
            attempt = f"{ques} has not been attempted. 🤨"
            print(attempt)

        time.sleep(0.25)
    if npassed == len(answers):
        print("Congratulations! You've answered all the questions correctly! 🎉🎉🎉")
        print("You can move onto the next module. 💯💯💯")



def assignment_01_check():
    answers = [
        ("first_char", "A", "1a"),
        ("last_char", " ", "1b"),
        ("third_char", "O", "1c"),
        ("record", "ATOM  ", "2a"),
        ("cleaned_record", "ATOM", "2b"),
        ("standard_line", True, "2c"),
        ("xcoord", "24.377", "3a [xcoord]"),
        ("ycoord", "59.531", "3a [ycoord]"),
        ("zcoord", "4.068", "3a [zcoord]"),
        ("coords", "24.377,59.531,4.068", "3b"),
        ("coord_sum", 87.976, "3c")
    ]
    assignment_check_helper(grandparent_locals(), answers)

def assignment_02_check():
    def a2_xcoord(x):
        return float(x[30:38])
    # pdb_file = os.path.join(data_dir, "assignment-02.pdb")
    vdict = grandparent_locals()
    ys = vdict['ys'].copy()
    if len(ys) < 8:
        del vdict['ys']

    seq_list_answer = []
    for header in vdict['headers']:
        if header[:6] == "SEQRES":
            seq_list_answer = seq_list_answer + header[19:].strip().split(" ")

    x_sorted_coords_answer = sorted(vdict['coords'], key = a2_xcoord)

    answers = [
        ("n_coords_strlist", ['24.377', '59.531', '4.068'], "1a"),
        ("ys", [59.531, 58.492, 58.393, 59.41, 58.862, 58.189, 59.094, 60.709], "1b"),
        ("yrange", [min(ys), max(ys)], "1c"),
        ("yrange_index", [ys.index(min(ys)), ys.index(max(ys))], "1d"),
        ("exp_data", "X-RAY DIFFRACTION", "2a"),
        ("seq_list", seq_list_answer, "2b"),
        ("aa3to1", bioseq.aa3to1, "2c"),
        ("seq", ''.join([bioseq.aa3to1(seq) for seq in seq_list_answer]), "2d"),
        ("get_xcoord", "get_xcoord(coords[0])==24.377", "3a"),
        ("x_sorted_coords", x_sorted_coords_answer,"3b")
    ]

    assignment_check_helper(vdict, answers)
    vdict['ys'] = ys
