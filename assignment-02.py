#------------------------------------------------------------------------------#
#          Script initialization - do not modify code in this section          #
#------------------------------------------------------------------------------#
import os
script_path = os.path.realpath(__file__)
tutorial_dir = os.path.dirname(script_path)
data_dir = os.path.join(tutorial_dir, "data")

os.sys.path.append(tutorial_dir)
from helpers import assignment_02_check
#------------------------------------------------------------------------------#

# The following are the coordinates of the nitrogen atom from the given PDB
# file. These will be used for some questions.
n_coords_str = "24.377,59.531,4.068"

# This variable contains all y-coordinates except that of the N atom.
ys = [58.492, 58.393, 59.410, 58.862, 58.189, 59.094, 60.709]


# The variable `header_file` contains the path to a toy PDB file with headers.
header_file = os.path.join(data_dir, "assignment-02-header.pdb")

def clean_line(line):
    """
    This removes newlines from the end of the string. This can handle both
    the Unix "\n" newlines and Windows "\r\n" carriage returns.
    """
    return line.rstrip("\n").rstrip("\r")

with open(header_file, "r") as fcon:
    headers = [clean_line(l) for l in fcon.readlines()]

coords_file = os.path.join(data_dir, "assignment-02-coords.pdb")
with open(coords_file, "r") as fcon:
    coords = [clean_line(l) for l in fcon.readlines()]


#------------------------------------------------------------------------------#
#                                 Question 01                                  #
#------------------------------------------------------------------------------#
#     (a) The variable `n_coords_str` contains the <x,y,z> coordinates of      #
#         a nitrogen atom. We will be doing arithmetic using these             #
#         coordinates, so it would be more convenient if they were stored      # 
#         as the elements of a list. Make a variable `n_coords_strlist`        # 
#         with the <x,y,z> coordinates in such a format. Do not convert the    # 
#         datatype of the elements.                                            # 
#                                                                              # 
#     (b) Extract the y-coordinate from `n_coords_strlist`, convert it to a    #
#         floating point number and add it to the beginning of `ys`.           # 
#         Hint: Use the `insert` method for lists.                             # 
#                                                                              # 
#     (c) Create a list `yrange` whose first element is the minimum of `ys`    # 
#         and whose second element is the maximum of `ys`. Use the `min` and   #
#         and `max` functions.                                                 #
#                                                                              #
#     (d) Create a list `yrange_index` whose first element is the index of the #
#         minimum value in `ys` and second element is the index of the         #
#         maximum value from `ys`.                                             #
#         Hint: Use the `index` method for lists.                              #
#                                                                              #
#------------------------------------------------------------------------------#

"""                          Write your answer here                         """


#------------------------------------------------------------------------------#
#                                 Question 02                                  #
#------------------------------------------------------------------------------#
#     (a) You are given a toy header from a PDB file. This has been loaded     #
#         and given to you as a list of strings called `headers`. Use the      # 
#         information given in this header to identify the experimental method # 
#         used to generate this PDB file. Extract this information from the    # 
#         EXPDTA record and store it under the variable `exp_data`. Use a `for`# 
#         loop to iterate through `headers` and subset the required line after # 
#         doing a check with an `if` statement. The link to the EXPDTA         #
#         format is given below. Make sure to strip trailing whitespace from   # 
#         your answer.                                                         # 
#                                                                              # 
#     (b) Using the information in the `SEQRES` record, create a `seq_list`    #
#         variable whose each element is a three-letter amino acid code of the # 
#         protein sequence.                                                    # 
#                                                                              # 
#     (c) Import the function aa3to1() from the `bioseq` module. This function # 
#         converts three-letter amino acid codes into one-letter codes.        # 
#                                                                              # 
#     (d) Using the aa3to1(), convert the strings in `seq_list` into           #
#         a single string `seq` containing the full sequence where the residues#
#         are represented by their one-letter codes. There are multiple methods#
#         to do this - string concatenation and the join() method for lists    # 
#         are two possibilities you could try.                                 # 
#                                                                              #
#------------------------------------------------------------------------------#

"""
https://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#EXPDTA
"""

"""                          Write your answer here                         """


#------------------------------------------------------------------------------#
#                                 Question 03                                  #
#------------------------------------------------------------------------------#
#     (a) You are given a toy coordinates from a PDB file. This has been       #
#         loaded and given to you as a list of strings called `coords`.        # 
#         Write a function called `get_xcoord` that extracts the x-coordinate  # 
#         from each line. The function should return value of type <float>.    # 
#                                                                              # 
#     (b) The lines you are given in `coords` are sorted by serial number.     #
#         Define a new variable called `x_sorted_coords` that has the lines    # 
#         sorted by the x coordinate. Use the `get_xcoord` function you        # 
#         wrote in the previous subquestion for this. The `sorted` function    # 
#         with the `key` option will be useful for this.                       # 
#                                                                              #
#------------------------------------------------------------------------------#

assignment_02_check()
