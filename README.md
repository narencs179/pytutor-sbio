# PyTutor - Structural Biology

This repository is intended to help individuals familiar with the basics of
Python programming to make the next leap of proficiency. It is themed around
completing assignments related to structural biology, specifically protein
structures and the ligand binding sites they possess.

To finish these assignments, you require:
1. A text editor to write your Python scripts.
2. A command line interface to execute these scripts.

These assignments are built around the assumption that you will use a simple
text editor. If you have a Linux installation, you probably have one of
gedit/KWrite/Kate installed, and you should use those. One Windows, try using
something like [https://notepad-plus-plus.org](Notepad++) or [https://www.sublimetext.com/](Sublime Text).
If you're feeling adventurous and want to do everything from the command line,
you could try vim/emacs.

For Windows users, we recommend running the assignments on Windows Subsystem for Linux (WSL),
although Powershell/Command Prompt on Windows can also make do.

For the most part, you will not need to install and use any external packages.
This repository aims to be as self-contained as possible.
