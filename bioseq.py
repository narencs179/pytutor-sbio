_aa1lst = [
    # 20 standard Amino Acids
    "A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q", "R",
    "S", "T", "V", "W", "Y",
    # 1 unknown
    "X",
]

_aa1to3dct = {
    "A": "ALA", "C": "CYS", "D": "ASP", "E": "GLU", "F": "PHE", "G": "GLY",
    "H": "HIS", "I": "ILE", "K": "LYS", "L": "LEU", "M": "MET", "N": "ASN",
    "P": "PRO", "Q": "GLN", "R": "ARG", "S": "SER", "T": "THR", "V": "VAL",
    "W": "TRP", "Y": "TYR",

    # 1 unknown
    "X": "UNK",
}

def aa1to3(aa):
    if aa not in _aa1to3dct:
        raise ValueError(f"Invalid amino acid: [{aa}]")
    else:
        return _aa1to3dct[aa]

_aa3lst = [aa1to3(aa1) for aa1 in _aa1lst]
_aa3to1dct = {_aa1to3dct[aa1]: aa1 for aa1 in _aa1lst}

def aa3to1(aa):
    if aa not in _aa3to1dct:
        raise ValueError(f"Invalid amino acid: [{aa}]")
    else:
        return _aa3to1dct[aa]

