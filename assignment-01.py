#------------------------------------------------------------------------------#
#          Script initialization - do not modify code in this section          #
#------------------------------------------------------------------------------#
import os
script_path = os.path.realpath(__file__)
tutorial_dir = os.path.dirname(script_path)
os.sys.path.append(tutorial_dir)

from helpers import assignment_01_check
#------------------------------------------------------------------------------#

# You are given a variable called `line` which contains an ATOM record from a
# PDB
line = "ATOM      1  N   MET A   1      24.377  59.531   4.068  1.00 30.02           N  "


#------------------------------------------------------------------------------#
#                                 Question 01                                  #
#------------------------------------------------------------------------------#
#     (a) Get the first character from the string `line` and store it in a     #
#         variable called `first_char`.                                        #
#                                                                              #
#     (b) Get the last character from the string `line` and store it in a      #
#         variable called `last_char`.                                         #
#                                                                              #
#     (c) Get the third character from the string `line` and store it in a     #
#         variable called `third_char`.                                        #
#                                                                              #
#------------------------------------------------------------------------------#

"""                          Write your answer here                          """




#------------------------------------------------------------------------------#
#                                 Question 02                                  #
#------------------------------------------------------------------------------#
#    The PDB uses a fixed-width file format, where each line is exactly 80     #
#     characters, and different regions of each line represents different      #
#   information. The first six characters of each line represents the record   #
#       name, and provides information as to how the rest of the line is       #
#                                 interpreted.                                 #
#                                                                              #
#     (a) Extract the first six characters from the string `line` and store    #
#         it in a variable called `record`.                                    #
#                                                                              #
#     (b) Remove any trailing spaces from the variable `record`, and store     #
#         the result in a variable called `cleaned_record`.                    #
#         Hint - The string method called `strip()` can help with this.        #
#                                                                              #
#     (c) Calculate the length of the string `line` and store it in a          #
#         variable called `line_length`.                                       #
#                                                                              #
#     (d) The identity and spatial coordinates of atoms in structures is       #
#         stored in lines with the ATOM record. As a basic quality check,      #
#         such lines must be of exactly 80 characters and must start with the  #
#         substring "ATOM". Determine whether the variable `line` meets these  #
#         requirements. Use the `==` operator to check for equality of values. #
#         Use the `and` operator to combine the results of the two checks and  #
#         store the result of this check in a variable called `standard_line`. #
#                                                                              #
#------------------------------------------------------------------------------#

"""                          Write your answer here                          """




#------------------------------------------------------------------------------#
#                                 Question 03                                  #
#------------------------------------------------------------------------------#
#  The <x, y, z> coordinates in an ATOM line are stored in specific regions.   #
# This is mentioned in the documentation on the WWPDB website. The link to the #
#                       relevant webpage is given below.                       #
#                                                                              #
#     (a) Consult the WWPDB documentation for the ATOM line, and extract the   #
#         x, y, and z coordinates of the atom in `line`, and store them in     #
#         variables named `xcoord`, `ycoord`, and `zcoord` respectively.       #
#         Save these values as a string, but ensure that there are no          #
#         trailing whitespaces.                                                #
#                                                                              #
#     (b) Produce a string which concatenates the x, y, and z coordinates into #
#         a single comma-separated string. For instance, if your coordinates   #
#         are x=1.23 y=2.34 z=3.45, your output should be "1.23,2.34,3.45".    #
#         Store this in a variable called `coords`.                            #
#         x, y, and z coordinates of the atom in `line`, and store them in     #
#         variables named `xcoord`, `ycoord`, and `zcoord` respectively.       #
#                                                                              #
#     (c) To carry out arithmetic operations on the coordinates, they must     #
#         first be converted into an appropriate numerical type. Define a      #
#         variable `coord_sum` that has the sum of the x, y, and, z            #
#         coordinates.                                                         #
#                                                                              #
#------------------------------------------------------------------------------#
""" https://www.wwpdb.org/documentation/file-format-content/format33/sect9.html#ATOM """

"""                          Write your answer here                          """




#------------------------------------------------------------------------------#
assignment_01_check()
